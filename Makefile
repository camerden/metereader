OCAMLC = ocamlc
OCAMLOPT = ocamlopt

BIN = metereader
MODULES = src/metereader.ml
PACKAGES = str unix

$(BIN).opt: $(MODULES)
	$(OCAMLOPT) -o $@ - $(PACKAGES:%=%.cmxa) $(MODULES)

$(BIN).run: $(MODULES)
	$(OCAMLC) -o $@ - $(PACKAGES:%=%.cma) $(MODULES)

.PHONY: clean
clean:
	rm -f src/*.cmi src/*.cmx src/*.o
	rm -f metereader.opt metereader.run
