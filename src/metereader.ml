type meter_val = Numeric of float | Textual of string
type meter_reg = { register: string; value: meter_val; unit: string option }

let debug_mode = ref false

let debug format =
  let printf_fun =
    if !debug_mode then
      Printf.fprintf
    else
      Printf.ifprintf
  in
  printf_fun stderr format

let baudrate_change_delay = 300  (* ms *)

let setup_tty fd baudrate (* 7E1 *) =
  debug "Setting up tty...\n%!";
  let curr_settings = Unix.tcgetattr fd in
  let new_settings =
    Unix.{ curr_settings with
      c_ibaud = baudrate;
      c_obaud = baudrate;
      c_parodd = false;
      c_csize = 7;
      c_cstopb = 1;
      c_parenb = true;
      c_ixoff = false;
      (* -crtscts *)
      c_hupcl = false;
      c_ixon = false;
      c_opost = false;
      (* -onlcr *)
      c_isig = false;
      c_icanon = false;
      (* -iexten *)
      c_echo = false;
    } in
  if curr_settings <> new_settings then
    debug "Adjusting \"terminal\" parameters (%u,7E1)...\n%!" baudrate;
    Unix.tcflush fd Unix.TCIOFLUSH;
    Unix.tcsetattr fd Unix.TCSANOW new_settings;
    Unix.sleepf ((float_of_int baudrate_change_delay) /. 1000.)


let read_line_fd_nonblock ?(timeout = 1.) fd =
  debug "Reading: %!";
  let rec _scan_endline fd buf pos timeout =
    let _buf = ref buf
    and _pos = ref pos
    and start = Unix.gettimeofday () in
    try
      while !_pos < 1 || (Bytes.get !_buf (!_pos-1)) <> '\n' do
        (* Read until either EOF or fd blocks *)
        if ((Bytes.length !_buf) - !_pos) < 1 then
          (* extend buffer by 64 characters, expected lines are short *)
          _buf := Bytes.extend !_buf 0 64;
        match Unix.read fd !_buf !_pos 1 with
        | 0 -> raise End_of_file
        | n ->
          debug "%s%!" (Bytes.to_string @@ Bytes.sub !_buf !_pos n);
          _pos := !_pos + n
      done;
      Bytes.sub !_buf 0 !_pos
    with
    | Sys_blocked_io
    | Unix.(Unix_error (EAGAIN, _, _))
    | Unix.(Unix_error (EWOULDBLOCK, _, _)) ->
      begin
        let elapsed = Unix.gettimeofday () -. start in
        let rest = timeout -. elapsed in
        if rest <= 0. then
          (* timeout reached *)
          raise Sys_blocked_io;
        match Unix.select [fd] [] [] rest with
        | fdl, _, _ when List.mem fd fdl ->
          _scan_endline fd !_buf !_pos rest
        | _, _, _ ->
          raise Sys_blocked_io
      end
  in
  let buf = _scan_endline fd Bytes.empty 0 timeout in
  debug "\n%!";
  Bytes.to_string buf


let rec retry tries ?(intersleep = 0.) lambda =
  if tries < 1 then
    failwith "retries exhausted";
  try
    lambda ()
  with e ->
    debug "exc: %s\n%!" @@ Printexc.to_string e;
    debug "retries left: %u\n%!" (tries - 1);
    if intersleep >= 0. then
      Unix.sleepf intersleep;
    retry (tries - 1) ~intersleep lambda


let send_message (fd: Unix.file_descr) (msg: string) : unit =
  debug "Sending message: '%s'\n%!" msg;
  ignore (Unix.single_write fd (Bytes.of_string msg) 0 (String.length msg));
  Unix.tcdrain fd

let send_handshake fd = send_message fd "/?!\r\n"
let ack_handshake fd = send_message fd "\x06000\r\n"

let handshake fd =
  retry 3 ~intersleep:2. (fun () ->
      Unix.tcflush fd Unix.TCIFLUSH;
      send_handshake fd;
      Unix.tcdrain fd;
      let line = read_line_fd_nonblock ~timeout:3. fd |> String.trim in
      debug "Received line: %s\n%!" line;
      assert (Str.string_match (Str.regexp "^/LGZ") line 0);
      debug "Meter-ID: %s\n%!" line;
    );
  Unix.sleepf 0.5;
  ack_handshake fd

let parse_reg_value_line line : meter_reg =
  debug "Processing: %s\n%!" line;
  if not (Str.string_match (Str.regexp "^\\([0-9A-F]+\\(\\.[0-9A-F]+\\)+\\)( *\\([0-9A-Za-z.]*\\)\\(\\*\\([A-Za-z]+\\)\\)?)$") line 0) then
    failwith ("Received bad register line: " ^ line);

  { register=Str.matched_group 1 line;
    value=(
      let grp = Str.matched_group 3 line in
      if grp <> "" then
        try
          Numeric (float_of_string grp)
        with _ ->
          Textual grp
      else
        Textual "");
    unit=
      match Str.matched_group 5 line with
      | s -> Some s
      | exception Not_found -> None
  }

let interact (fd: Unix.file_descr) (callback: (string -> meter_reg option)) : meter_reg list =
  let rec collector accum =
    try
      let line = read_line_fd_nonblock ~timeout:3. fd |> String.trim in
      let new_accum =
        match callback line with
        | Some res -> res :: accum
        | None -> accum
      in
      collector new_accum
    with
    | End_of_file -> accum
  in
  collector []

let get_values (fd: Unix.file_descr) : meter_reg list =
  handshake fd;
  retry 3 ~intersleep:1. (fun () ->
      interact fd (fun line ->
          (* printf "Has line: %s\n" line; *)
          match line with
          | "!" -> raise End_of_file
          | "" -> None
          | s when Str.string_match (Str.regexp "^/LGZ") s 0 ->
             None (* XXX: Exception?? *)
          | s when Str.string_match (Str.regexp "^[0-9A-F.]+(.*)$") s 0 ->
             Some (parse_reg_value_line s)
          | s -> debug "Unknown line: %s\n" s; None)
    )

let rec join_list sep l =
  match l with
  | [] -> ""
  | [x] -> x
  | x :: xs -> x ^ sep ^ (join_list sep xs)


let () =
  (* Default values for command-line arguments. *)
  let devname = ref "" in
  let print_header = ref false in
  let print_units = ref true in
  let selected_registers_rev = ref [] in

  (* Command line argument processing. *)
  Arg.parse [
    ("-dev", Arg.Set_string devname, "the device");
    ("-units", Arg.Set print_units, "print out units (default)");
    ("-no-units", Arg.Clear print_units, "don't print out units");
    ("-debug", Arg.Set debug_mode, "enable debug output");
    ("-H", Arg.Set print_header, "print a header line");
    ("-r", Arg.String (fun s -> selected_registers_rev := [s] @ !selected_registers_rev), "registers (=columns) to print")
  ] (fun (s) -> ()) "usage";

  if !devname = "" then
    raise (Arg.Bad "-dev is required");
  if List.length !selected_registers_rev < 1 && not !print_header then
    Printf.eprintf "Column order is ambiguous\n";

  let fd = Unix.(openfile !devname [O_RDWR; O_NOCTTY; O_NONBLOCK; O_SYNC; O_RSYNC] 0) in
  try
    Unix.(lockf fd F_LOCK 0);

    (* Setup serial console. *)
    setup_tty fd 300;

    (* Collect measurements. *)
    let reg_values = get_values fd in

    let selected_cols =
      if List.length !selected_registers_rev > 0 then
        (* Print selected columns. *)
        let find_reg = fun (reg_name) ->
          List.find (fun reg -> reg.register = reg_name) reg_values
        in
        List.rev_map find_reg !selected_registers_rev
      else
        (* Select all registers. *)
        reg_values
    in
    let hdr, rows =
      List.map (fun reg ->
          (reg.register,
           match reg.value with
           | Textual s -> s
           | Numeric n ->
              match reg.unit with
              | Some s when !print_units -> (string_of_float n) ^ " " ^ s
              | Some s                   -> (string_of_float n)
              | None                     -> (string_of_float n)
        )) selected_cols
      |> List.split
    in
    (if !print_header then [hdr; rows] else [rows])
    |> List.map (join_list ",")
    |> List.iter print_endline;

    Unix.(lockf fd F_ULOCK 0);
    Unix.close fd
  with e ->
    Unix.(lockf fd F_ULOCK 0);
    Unix.close fd;
    raise e
